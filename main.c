#include <XC.h>
#include <sys/attribs.h>
#include <stdio.h>
#include <stdlib.h>

#include "uart.h"
#include "wheel.h"
#include "arm_servo.h"

#include "coin_detector.h"
#include "line_detector.h"

#include "movement.h"


// Configuration Bits (somehow XC32 takes care of this)
#pragma config FNOSC = FRCPLL       // Internal Fast RC oscillator (8 MHz) w/ PLL
#pragma config FPLLIDIV = DIV_2     // Divide FRC before PLL (now 4 MHz)
#pragma config FPLLMUL = MUL_20     // PLL Multiply (now 80 MHz)
#pragma config FPLLODIV = DIV_2     // Divide After PLL (now 40 MHz)
#pragma config FWDTEN = OFF         // Watchdog Timer Disabled
#pragma config FPBDIV = DIV_1       // PBCLK = SYCLK
#pragma config FSOSCEN = OFF        // Secondary Oscillator Enable (Disabled)

#define SYSCLK 40000000L


void setup_pins(){
//Set-up motor pins
	TRISBbits.TRISB0 = 0;
	TRISBbits.TRISB1 = 0;
	TRISBbits.TRISB2 = 0;
	TRISBbits.TRISB3 = 0;
	
	//Stop Motors
	LATBbits.LATB0 = 0;
	LATBbits.LATB1 = 0;
	LATBbits.LATB2 = 0;
	LATBbits.LATB3 = 0;
	
	
	//Set-up arm servo pins (magnet, base, and power resp.) and electromagnet power
	TRISAbits.TRISA2 = 0; 
	TRISAbits.TRISA3 = 0;
	TRISBbits.TRISB4 = 0;
	TRISAbits.TRISA4 = 0;
	
	//Kill Servo/Electromagnet Power
	LATAbits.LATA4 = 0;
	LATBbits.LATB4 = 0;
	
	
	//Set as analog input for line detector.
    ANSELBbits.ANSB12 = 1;   // set RB12 (AN12, pin 23 of DIP28) as analog pin
    TRISBbits.TRISB12 = 1;   // set RB12 as an input
	
	//Set digital input for coin detector.
	ANSELB &= ~(1<<6); // Set RB6 as a digital I/O
    TRISB |= (1<<6);   // configure pin RB6 as input
    CNPUB |= (1<<6);   // Enable pull-up resistor for RB6
    
    
    //Set-up manual/automatic buttons
    ANSELBbits.ANSB13 = 0;
	ANSELBbits.ANSB14 = 0;
	TRISBbits.TRISB13 = 1;
	TRISBbits.TRISB14 = 1;
	CNPUBbits.CNPUB13 = 1;
	CNPUBbits.CNPUB14 = 1;
	
	
	//Set-up Testing
	TRISBbits.TRISB10 = 0;	//LED Coin
	TRISBbits.TRISB15 = 0;	//LED Line
}



void main(void){
	int x,y,coin_count=0;
	DDPCON = 0;

	//Initial Configurations
	setup_pins();			//Configure I/O pins
	UART2Configure(9600);	//Configure UART2 for a baud rate of 9600 (HC-06 baud rate)
	waitms(1000);			//Let UART setup
	ADCConf();				//Set-up ADC first to allow volt_initialize
	
	//Sensor base values
	volt_initialize();
	waitms(500);
	freq_initialize();

	//Wheel and Arm Servo timers
	SetupTimer1();
	SetupTimer2();
	INTCONbits.MVEC = 1;	//Enable Multi-vector Interrupts

	
	//Wheel and Arm Servo default position
	LATBbits.LATB4 = 1;		//Turn on servo
	set_Wheel_Left(0,0);
	set_Wheel_Right(0,0);
	
	set_Arm_Base(62,25);
	set_Arm_Magnet(0,25);
	waitms(1000);
	LATBbits.LATB4 = 0;		//Turn off servo
	
	
	
	
	
	
	
	//Mode Select (RB13 for Manual, RB14 for Automatic)
	while(PORTBbits.RB13 && PORTBbits.RB14){
	}
	
	//Manual Mode
	if(!PORTBbits.RB13){
		speed = 75;
		while(1){
			SerialRespond();
			SerialTransmit("\r\n");
		}	
	}
	
	
	
	
	//Automatic Mode
	speed = 65;
	move_robot_fwd();
	
	//Infinite Loop
	while(1){
	
		//Monitors coin count, assumes only 20 coins in field
		if(coin_count>=20){
			stop_robot();
			while(1){
			}
		}
		
		//Coin Check
		y=0;
		if(coin_detect()) y = coin_detect();	//Double-Check frequency
		LATBbits.LATB10 = y;
		
		//If double-check passed, stop robot and check one more time
		if(y){
			stop_robot();
			waitms(250);

			//If third check passed, pick-up coin
			if(coin_detect()){
				move_robot_back();
				waitms(300);
				stop_robot();
				move_arm();
				coin_count++;
				waitms(200);
				freq_initialize();	//Assume cleared inductor, reset values
			}
			move_robot_fwd();	//Go back to moving forward
		}
		
		
		//Line Check
		if(line_detect()) if(line_detect()) x = line_detect();	//Triple-check voltage
		LATBbits.LATB15 = x;
		while(x){	//If triple-check passed, back-up and turn right
			stop_robot();
			waitms(200);
			move_robot_back();
			waitms(1000);
			rotate_robot_right();
			waitms(1000);
			x = line_detect();	//Keep turning until no voltage present
		}
		move_robot_fwd();	//Maintain going forward
	}
}