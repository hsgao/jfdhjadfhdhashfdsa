#include <XC.h>


#define MAGNET_POWER LATAbits.LATA4
#define SERVO_POWER LATBbits.LATB4

volatile unsigned int speed=0;



void move_robot_fwd(void){
	set_Wheel_Left(speed,0); 
	set_Wheel_Right(speed+3,0); 
}

void move_robot_back(void){
	set_Wheel_Left(speed-10,1); 
	set_Wheel_Right(speed-5,1); 
}

void rotate_robot_right(void){
	set_Wheel_Left(speed,0); 
	set_Wheel_Right(speed+5,1); 
}

void rotate_robot_left(void){
	set_Wheel_Left(speed,1);
	set_Wheel_Right(speed+5,0);
}


void stop_robot(void){
	set_Wheel_Left(0,0); 
	set_Wheel_Right(0,0); 
}

void move_arm(void) {
	SERVO_POWER = 1;
	set_Arm_Base(65,19); 
	set_Arm_Magnet(165,19); //Move arm to middle and move magnet down
	MAGNET_POWER = 1;
	delay_ms(1000);
	
	set_Arm_Base(90,19);
	delay_ms(500);
	
	set_Arm_Base(65,19); //Sweeping motion to pick up coin 
	set_Arm_Magnet(10,19); //Move arm up 
	delay_ms(1000);
	
	//Move and drop coin 
	set_Arm_Base(35,19); 
	set_Arm_Magnet(45,19); 
	delay_ms(500);
	MAGNET_POWER = 0;
	delay_ms(500);
	
	//NEED TO DISENGAGE MAGNET HERE 

	set_Arm_Base(65,19); 	
	set_Arm_Magnet(10,19); //Initial Pos. 
	delay_ms(500);
	SERVO_POWER = 0;
	delay_ms(500);
	
	
}




void SerialRespond(void){
	long int count=0;
	char input;
	while(!U2STAbits.URXDA){
		count++;
		if(count>=500000){
			input = '0';
			break;
		}
	}
	if(count<500000) input = U2RXREG;
	switch(input){
		case 'w': move_robot_fwd();;
		SerialTransmit("w");
		break;
		case 'a': rotate_robot_left();
		SerialTransmit("a");
		break;
		case 's': move_robot_back();
		SerialTransmit("s");
		break;
		case 'd': rotate_robot_right();
		SerialTransmit("d");
		break;
		case ' ': move_arm();
		SerialTransmit("space");
		break;
		default: stop_robot();
		SerialTransmit("stop");
		break;
	}
}

	
